# ------------------------------------------------------------------
# File name : Final Assignment Python Script
# ------------------------------------------------------------------
# Group number    : 7
# Group members   : Eoghan Conlon, Mark Sheridan
# Last updated on : 27/11/2023
# ------------------------------------------------------------------
# Module description:
# This Module is used in the final project for this module, it runs
# the interface from the Uno to our laptop over one of the serial
# ports. It also saves the readings from the sensor in a csv file
# along with running a test process over the LED. The interface is
# run using either a command line like interface or a speech
# recognition interface, which is chosen on runtime.
# -----------------------------------------------------------------

# ------------------------------------------------------------------
# Modules to import
# ------------------------------------------------------------------
import csv

import serial
import time
import speech_recognition as sr
import pyaudio
import pyttsx3
from word2number import w2n
import globals
import functions


# ------------------------------------------------------------------
# def main()
# ------------------------------------------------------------------
# Main script function
# ------------------------------------------------------------------


def main():
    results = []
    ser = serial.Serial(com_port)
    ser.baudrate = 9600
    ser.flush()
    ser.timeout = 5
    ser.readline()

    # ------------------------------------------------------------------
    # def sample()
    # ------------------------------------------------------------------
    # Function description:
    # This function is a function that was broken out of both the command
    # line handler and the speech recognition parts to cut down on code
    # duplication.
    # This function is inside main as it requires functions that only
    # exist in main.
    # ------------------------------------------------------------------
    def sample():
        ser.write('s'.encode())
        i = 0
        while i < 10:
            result = ser.readline().decode()
            if result != '':
                results.append(result.split())
                i += 1

    # ------------------------------------------------------------------
    # def save()
    # ------------------------------------------------------------------
    # Function Description:
    # This function is another bit of common code between the two
    # different interfaces to cut down on code duplication.
    # The function is inside main to save on passing variables
    # uneccesarily.
    # ------------------------------------------------------------------
    def save():
        with open('../../Voice_readings/sample5.csv', 'w+', newline='') as csvfile:
            filewriter = csv.writer(csvfile, dialect='excel', delimiter=' ', quotechar='|',
                                    quoting=csv.QUOTE_MINIMAL)
            for each in results:
                filewriter.writerow(each)

    # ------------------------------------------------------------------
    # def test()
    # ------------------------------------------------------------------
    # Function Description:
    # This function is common code between the two interfaces, split out
    # to avoid code duplication. It is located within main as it uses
    # variables managed by main.
    # ------------------------------------------------------------------
    def test():
        ser.write('r'.encode())
        time.sleep(0.5)
        ser.write('o'.encode())
        ser.write('b'.encode())
        time.sleep(0.5)
        ser.write('o'.encode())
        ser.write('g'.encode())
        time.sleep(0.5)
        ser.write('o'.encode())

    r = sr.Recognizer()

    vcoption = input("Choose 1 for voice commands 0 for keyboard commands\n")

    match vcoption:
        case "1":
            voice = 1
        case "2":
            voice = 0
        case _:
            say_something("Error")

    while voice:
        with sr.Microphone(device_index=2) as source:
            say_something('Say something ...')
            audio = r.listen(source)

        try:
            user_input = r.recognize_google(audio).lower()
            match user_input:
                case 'red':
                    ser.write('r'.encode())
                case 'green':
                    ser.write('g'.encode())
                case 'blue':
                    ser.write('b'.encode())
                case 'led off':
                    ser.write('o'.encode())
                case 'test':
                    say_something("LED test sequence started")
                    test()
                    say_something("LED test sequence finished")
                case 'quit':
                    ser.write('q'.encode())
                    while ser.readline().decode() != '':
                        say_something(ser.readline().decode())
                    break
                case 'save':
                    say_something('Saved')
                    save()
                case 'sample':
                    sample()
                case 'run':
                    with sr.Microphone(device_index=2) as source:
                        say_something('How many samples do you want')
                        audio = r.listen(source)

                    try:
                        runs_voice = r.recognize_google(audio)
                        print(runs_voice)
                        runs = w2n.word_to_num(runs_voice)
                        print(runs)
                    except sr.UnknownValueError:
                        say_something(sr.UnknownValueError)

                    except sr.RequestError as e:
                        say_something(
                            'Could not request results from ' 'Google Speech Recognition service; {0}'.format(e))

                    with sr.Microphone(device_index=2) as source:
                        say_something('What delay do you want')
                        audio = r.listen(source)

                    try:
                        delay_voice = r.recognize_google(audio)
                        print(delay_voice)
                        delay = w2n.word_to_num(delay_voice)
                        print(delay)
                    except sr.UnknownValueError:
                        say_something(sr.UnknownValueError)

                    except sr.RequestError as e:
                        say_something(
                            'Could not request results from ' 'Google Speech Recognition service; {0}'.format(e))

                    i = 1
                    while i <= runs:
                        results.append('Sample ' + str(i))
                        sample()
                        say_something('Sample ' + str(i) + ' complete')
                        if i < 5:
                            time.sleep(delay)
                        i += 1

                case 'help':
                    say_something('Arduino Interface Help: ')
                    say_something('red: Turn the red LED on')
                    say_something('blue: Turn the blue LED on')
                    say_something('green: Turn the green LED on')
                    say_something('off: Turn the LEDs off')
                    say_something('test: Turn each LED on for half a second before turning them off again')
                    say_something('sample: Collect one ample from the arduino')
                    say_something('run: Collect [samples] with [delay]s delay')
                    say_something('    samples: Amount of samples required (Requested after command is started)')
                    say_something('    delay: Time delay between samples (Requested after command is started)')
                    say_something('save: Save the contents of the results list as a CSV')
                    say_something('help: runs this command')
                    say_something('quit: Exits the script')
                case _:
                    print(user_input)
                    say_something("user_input")
        except sr.UnknownValueError:
            say_something(sr.UnknownValueError)

        except sr.RequestError as e:
            say_something('Could not request results from ' 'Google Speech Recognition service; {0}'.format(e))

    while not voice:
        option = input("Please input a command:\n")
        match option:
            case 'red':
                ser.write('r'.encode())
            case 'green':
                ser.write('g'.encode())
            case 'blue':
                ser.write('b'.encode())
            case 'off':
                ser.write('o'.encode())
            case 'quit':
                ser.write('q'.encode())
                while ser.readline().decode() != '':
                    print(ser.readline().decode())
                break
            case 'sample':
                sample()
            case 'test':
                test()
            case 'save':
                save()
            case 'run':
                num_of_samples = int(input("How many samples do you want:\n"))
                delay_in_seconds = int(input("What delay do you want (in seconds):\n"))
                i = 1
                while i <= num_of_samples:
                    results.append('Sample ' + str(i))
                    sample()
                    print("Sample ", i, " complete")
                    i += 1
                    if i < 5:
                        time.sleep(delay_in_seconds)
            case 'help':
                print('Arduino Interface Help: ')
                print('red: Turn the red LED on')
                print('blue: Turn the blue LED on')
                print('green: Turn the green LED on')
                print('off: Turn the LEDs off')
                print('test: Turn each LED on for half a second before turning them off again')
                print('sample: Collect one ample from the arduino')
                print('run: Collect [samples] with [delay]s delay')
                print('    samples: Amount of samples required (Requested after command is started)')
                print('    delay: Time delay between samples (Requested after command is started)')
                print('save: Save the contents of the results list as a CSV')
                print('help: runs this command')
                print('quit: Exits the script')
            case _:
                print('Invalid command.')


# ------------------------------------------------------------------
# Module is only run as a script or with python -m, but not
# when it is imported.
# ------------------------------------------------------------------
if __name__ == '__main__':
    main()
# ------------------------------------------------------------------
# End of script
# ------------------------------------------------------------------
